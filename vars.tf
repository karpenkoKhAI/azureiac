variable "env_id" {
  description = "The ID of enviroment"
  type        = string
  default     = "qa1"
}

variable "location" {
  description = "The specific region in MS Azure"
  type        = string
  default     = "westus2" #West US
}

variable "vnet_cidr" {
  description = "The vnet CIDR"
  type        = string
  default     = "10.110.0.0/16"
}

variable "azureuser_password" {
    default = "Abc47483!93_"
}

variable "vms" {
  description = "The map of vms properties"
  type        = map(any)
  default = {
    vm1 = {
      size                 = "Standard_F2"
      caching              = "ReadWrite"
      storage_account_type = "Standard_LRS"
      publisher            = "MicrosoftWindowsServer"
      offer                = "WindowsServer"
      sku                  = "2016-Datacenter"
      version              = "latest"
      zone                 = "1"
    }
    vm2 = {
      size                 = "Standard_F2"
      caching              = "ReadWrite"
      storage_account_type = "Standard_LRS"
      publisher            = "MicrosoftWindowsServer"
      offer                = "WindowsServer"
      sku                  = "2016-Datacenter"
      version              = "latest"
      zone                 = "2"
    }
  }
}





variable "tags" {
  type = map(string)
  default = {
    "Owner"      = "andrii.karpenko1@gmail.com"
    "CostCenter" = "8904"
  }
}
