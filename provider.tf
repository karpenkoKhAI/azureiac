provider "azurerm" {
  subscription_id = "430e699a-e1e3-441b-a0fa-b47582a27a16"
  tenant_id       = "216765f0-b226-4162-ad3a-1498a1897c16"

  features {}
}

terraform {
  required_version = "= 0.14.6"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.65.0"
    }
  }
}
